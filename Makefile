.DEFAULT: help

DC_PROD := docker compose -f docker-compose.yml
DC_DEV := $(DC_PROD) -f docker-compose.dev.override.yml

.PHONY: help
help:
	@ grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## Run all the tests
	@ $(DC_DEV) run --rm ui pytest . tests

.PHONY: code_coverage
code_coverage: ## Estimate the code coverage rate of tests
	@ cd src && poetry run coverage run -m pytest . tests && poetry run coverage html

.PHONY: black
black: ## Run linter tool black
	@ $(DC_DEV) run --rm ui poetry run black --verbose -- .

.PHONY: isort
isort: ## Run linter tool isort
	@ $(DC_DEV) run --rm ui poetry run isort . --check-only

.PHONE: flake8
flake8: ## Run linter tool flake8
	@ $(DC_DEV) run --rm ui poetry run flake8 --verbose .

.PHONY: serve_dev
serve_dev: ## Serves application in a development environment
	@ $(DC_DEV) up --build

.PHONY: serve_prod
serve_prod: ## Serves application in a production environment (as a daemon)
	@ $(DC_PROD) up -d --build
